FROM nginx:alpine
WORKDIR /app
ADD . .
RUN mv dist/nginx.conf /etc/nginx/nginx.conf && rm -r dist