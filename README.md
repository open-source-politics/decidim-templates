# decidim-templates
CSS themes for Decidim to be injected using the header snippet accessible in admin.

## Glossary

- `Template` : A template is made of **variables** to which you can assign values that will affect the styling of the platform. It's also made of CSS code that implement these variables on the relevant CSS classes.
- `Themes` : A theme is just a set of values given to the variable that create a coherent graphic universe.


## Theme list

- [Slick](/templates/main/slick/)
- [Flashy](/templates/main/flashy/)
- [Marianne](templates/instit/marianne/)
- [Institutionnel](templates/instit/institutionnel/)
- [OSP](templates/demo/osp/)

## How to use a theme
Paste the content of the `header_snippet_[theme].html` file of the theme you want to apply in the header snippet of your Decidim instance  `https://YOURDECIDIM/admin/organization/appearance/edit` and then add the `variables` you want by creating a `client_name.css` in the `clients/client_name/` folder. Then you can copy paste content from the `variables.css` of the choosen theme inside this new `client_name.css`

![header snippet](https://opensourcepolitics.eu/wp-content/uploads/2021/10/header_snippet.png)

## Getting started

### Create a custom theme for a Decidim instance

#### Create a new theme

If you want to create a new template called `myTemplate` with a theme called `myTheme`, you will create this architecture:

1. Create a new folder called `template_myTemplate`

```bash
mkdir template_myTemplate
```

2. In folder `template_myTemplate`, create two files called `header_snippet_myTemplate.html` and `template_myTemplate.css`. If you want animations, you can create a third one called `animations_myTemplate.js`.

```bash
touch template_myTemplate/header_snippet_myTemplate.html
touch template_myTemplate/template_myTemplate.css
touch template_myTemplate/animations_myTemplate.js
```

3. Create your new theme `myTheme`

```bash
mkdir template_myTemplate/myTheme
```

4. Add the needed files to your theme `myTheme`

```
touch template_myTemplate/myTheme/README.md
touch template_myTemplate/myTheme/variables.css
```

Congratulations, you can now work on your new theme !

## Template architecture
```
myTemplate
├── theme1
│     ├── README.md
│     └── variables.css
├── theme2
│     ├── README.md
│     └── variables.css
├── animations_myTemplate.js
├── header_snippet.html
└── template_myTemplate.css
```
- `theme1` : Theme folder
  - `README.md` : Readme file that explains how the theme works and a link to a decidim using this template.
  - `variables.css` : CSS file that contains all variables of the themes.
- `theme2` : Theme folder
  - `README.md` : Readme file that explains how the theme works and a link to a decidim using this template.
  - `variables.css` : CSS file that contains all variables of the themes.
- `animations_myTemplate.js` : File that contains the animations used by all children themes.
- `header_snippet.html` : File that contains the HTML template used by all children themes.
- `template_myTemplate.css` : File that contains the CSS used by all children premium themes.

## How to create a custom theme for a Decidim instance

- Open a text editor (We recommend [Atom](https://atom.io/) if you don't have one already)
- Open a new file `client_name.html`
- Copy paste the content of the `header_snippet.html` from the template of your choice.
- Copy paste the content of the `variables.css` from the template of your choice inside the `root` section (at the bottom of your `client_name.html`)
- Give different values to the variables.
- Copy & paste the whole code inside the header snippet field of your instance, click on update and check how the changes applied on the public side.

## Features

- Web animations : Themes use the aos library to animate on scroll contents on the page.
- Google fonts : fonts can be imported directly from the template using google font.

**⚠ If you want to use a font that's not available on google font**
- Make sure you have the **.otf** files for your font
- Ask the developpers to add the font on the decidim-app
- Remove the google font import on the theme you are using
- Replace the font in the variables `--font_title` and `--font_content` by the font you added.

## How to contribute

### Create a theme

- Git checkout on `develop`
- Create a branch `feature/add-theme-[theme-name]` or `fix/describe-behaviour-to-fix`
- Copy the set of variables contained in `root` from the source template (or an existing one) and start working on creating a coherent theme by assigning values to the variables

### Add a variable

Missing a variable ?
- Git checkout on `develop`
- Create a branch `feature/add-variable-[variable-name]`
- Create the variable `--variable_name` in the proper section (header, footer etc.)
- Implement the variable in the proper CSS code section (buttons, header etc.)


## Releases

Current version is `v2.0.0` it should be compatible with Decidim versions from `0.18` to `0.25`.

We use semantic releases (vX.X.X -> Major.Minor.Patch)
Examples :
- Major : Large update on the source template and the themes (could happen with a decidim version upgrade for example)
- Minor : Add a theme, add a variable
- Patch : Bug fix, documentation

### Source theme coherence

In order to avoid huge differences between themes in terms of what variables are available and what CSS classes implement the variable we use the source template as a reference that has all the variables and implements all classes.

The source template is where the enhancements and fixes are made. It serves as a template to create themes.

**It can very tempting to quickly add a variable or a css class on a theme to satisfy a need. PLEASE DO NOT PROCEDE THIS WAY**

Every time you need to add / change / delete a variable **make sure you do it on the source template FIRST**.

Every time you need to add / change / delete a CSS class implementation **make sure you do it on the source template FIRST**.

When done **add the variable to the themes css files**.