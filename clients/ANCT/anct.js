window.addEventListener("DOMContentLoaded", (event) => {

    // BURGER MENU - MOBILE
    let menuOpen = document.getElementById('button-479')
    let modal = document.getElementById('modal-478')
    let menuClose = modal.querySelector('button.fr-btn--close.fr-btn')

    menuOpen.addEventListener('click', function() {
        modal.classList.add("fr-modal--opened")
    })

    menuClose.addEventListener('click', function() {
        modal.classList.remove("fr-modal--opened")
    })


    // HIDE TEXT ON SPECIFIC URL
    let currentURL = window.location.href
    let targetURL = "https://anct.osp.cat/assemblies/concertationslocales"

    if (currentURL === targetURL) {
        let parent = document.querySelector("#content .wrapper .row.column .row .columns.medium-7.mediumlarge-8 .section")
        let children = parent.children

        for (let child of children) {
            let tagName = child.tagName.toLowerCase()
            if (tagName !== 'h2' && tagName !== 'div') {
                child.style.display = 'none'
            }
        }
    }

})