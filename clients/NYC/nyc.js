window.addEventListener("DOMContentLoaded", (event) => {
    try {
        // ADD ALL PROPOSALS BUTTON IN EXPLORE PROPOSALS
        let clearFilters = document.querySelector(".clear-filters")
        console.log(clearFilters);
        let columnMediumLarge = document.querySelector(".columns.mediumlarge-12.large-3")
        console.log(columnMediumLarge);
        columnMediumLarge.removeChild(clearFilters)

        let filtersSection = document.createElement("div")
        filtersSection.classList.add("filters__section")
        columnMediumLarge.appendChild(filtersSection)

        let newFieldset = document.createElement("fieldset")
        newFieldset.style.textAlign = "right"
        filtersSection.appendChild(newFieldset)

        let newLegend = document.createElement("legend")
        newLegend.classList.add("mini-title")
        newLegend.innerHTML = "&nbsp"
        newFieldset.appendChild(newLegend)

        let button = document.createElement("a")
        button.classList.add("button")
        button.classList.add("large")
        button.classList.add("button--sc")
        button.style.background = "var(--primary)"
        button.style.color = "#000000"
        button.style.textTransform = "uppercase"
        button.style.fontFamily = "museo !important"
        button.style.fontWeight = "600"
        button.style.padding = "10px 16px"
        button.innerHTML = "All Proposals"
        newFieldset.appendChild(button)
    } catch (error) {
        console.error("Une erreur est survenue pour la fonctionnalité ADD ALL PROPOSALS BUTTON IN EXPLORE PROPOSALS :", error);
    }

    // ENDORSEMENT BUTTON
    if (window.location.href.includes('https://www.participate.nyc.gov/processes/Citywidepb2023/f/321/proposals/')) {
        const url = window.location.href;
        const proposalId = url.match(/proposals\/(\d+)$/);
        if (proposalId && proposalId[1]) {
            const extractedId = proposalId[1];
            const endorsementsCount = document.getElementById(`resource-${extractedId}-endorsements-count`);
            const endorsementsButton = document.querySelector('button.button.button.small.compact.light.button--sc.expanded.secondary');
            const endorsements = document.querySelector('.button-group.button-group--collapse.button--nomargin.small')
            const followingButton = document.querySelector(`div#follow_proposal_${extractedId}`)
            const followingIcon = followingButton.querySelector('span.icon-wrap')
            if (endorsementsCount) {
                endorsementsCount.style.background = 'transparent';
                endorsementsCount.style.color = 'black';
                endorsementsCount.style.border = 'solid 1px black';
                endorsementsCount.style.display = 'flex';
                endorsementsCount.style.alignItems = 'center';
                endorsementsCount.style.width = '53px';
            }

            if (endorsementsButton) {
                endorsementsButton.style.background = 'transparent';
                endorsementsButton.style.color = 'black';
                endorsementsButton.style.border = 'solid 1px black';
                endorsementsButton.style.borderLeft = 'hidden'
                endorsementsButton.style.fontWeight = '400'
            }

            endorsements.addEventListener('mouseover', function() {
                endorsementsButton.style.background = 'black';
                endorsementsButton.style.color = 'white';
                endorsementsCount.style.background = 'black';
                endorsementsCount.style.color = 'white';
            });
            
            endorsements.addEventListener('mouseout', function() {
                endorsementsButton.style.background = 'transparent';
                endorsementsButton.style.color = 'black';
                endorsementsCount.style.background = 'transparent';
                endorsementsCount.style.color = 'black';
            });

            if (followingIcon) {
                followingIcon.style.display = 'flex';
                followingIcon.style.alignItems = 'center'
                followingIcon.style.width = '51.5px';
            }
        }
    }
});
