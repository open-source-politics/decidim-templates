window.addEventListener("DOMContentLoaded", (event) => {
    // FOOTER 

    let mainFooter = document.querySelector(".main-footer")
    let footerRight = mainFooter.querySelector("div.medium-8.large-6.large-offset-3.column")
    let footerLeft = mainFooter.querySelector("div.medium-4.large-3.column")

    let miniFooter = document.querySelector(".mini-footer")
    let copyrightOSP = miniFooter.querySelector(".mf-text")
    
    footerRight.classList.remove("large-6")
    footerRight.classList.add("large-3")
    footerRight.classList.remove("large-offset-3")
    footerRight.classList.add("large-offset-6")

    let p = document.createElement("p")
    p.innerHTML = "Suivez-nous"
    p.style.marginTop = "1rem"
    p.style.marginBottom = "0"
    p.style.textTransform = "uppercase"
    p.style.color = "var(--primary)"
    footerLeft.insertBefore(p, footerLeft.firstChild)

    let a = document.createElement("a")
    a.setAttribute("href", "/pages/cese")
    a.innerHTML = "Site du CESE"
    footerLeft.insertBefore(a, footerLeft.firstChild)

    copyrightOSP.style.marginRight = "10px"
    let copyrightPhoto = document.createElement("div")
    copyrightPhoto.classList.add("mf-text")
    copyrightPhoto.innerHTML = "Crédit photo © Katrin Baumann"
    let row = miniFooter.querySelector(".row")
    row.appendChild(copyrightPhoto)

    // ______________________________________________________________________

    // INITIATIVES TITLE

    let currentURL = window.location.href
    if (currentURL === "https://petitions.lecese.fr/initiatives") {
        let mainContent = document.querySelector("main#content")
        let wrapper = mainContent.querySelector("div.wrapper")

        let divWrapper = document.createElement("div")
        divWrapper.classList.add("row")
        divWrapper.classList.add("column")

        let pageTitleWrapper = document.createElement("div")
        pageTitleWrapper.classList.add("page-title-wrapper")
        divWrapper.appendChild(pageTitleWrapper)

        let antiFlexDiv = document.createElement("div")
        antiFlexDiv.classList.add("anti-flex-div")
        pageTitleWrapper.appendChild(antiFlexDiv)

        let h1 = document.createElement("h1")
        h1.classList.add("heading1")
        h1.classList.add("page-title")
        h1.innerHTML = "Bienvenue dans<br>l’espace des pétitions!"
        antiFlexDiv.appendChild(h1)

        let p1 = document.createElement("p")
        p1.classList.add("custom-title-p")
        p1.innerHTML = "C’est ici que vous pouvez parcourir les pétitions déjà en ligne, les soutenir et déposer de nouvelles pétitions.<br>Pour en savoir plus sur le dépôt ou la signature de pétition, ou encore le traitement des pétitions au CESE, rendez-vous sur la page <a href=\"/pages/initiatives\">Pétitions : mode d'emploi</a>."
        let p2= document.createElement("p")
        p2.classList.add("custom-title-p")
        p2.innerHTML = "Besoin d’aide ? Contactez-nous à l’adresse <a href=\"mailto:petitions@lecese.fr\">petitions@lecese.fr</a>"
        antiFlexDiv.appendChild(p1)
        antiFlexDiv.appendChild(p2)

        wrapper.insertBefore(divWrapper, wrapper.firstChild)

        antiFlexDivBefore = window.getComputedStyle(antiFlexDiv, "::before")
        antiFlexDivHeight = antiFlexDiv.offsetHeight
        root = document.querySelector(":root");
        root.style.setProperty("--antiflexdivheight", antiFlexDivHeight+"px");

        // __________________________________________________________________

        let svgIcon = `
        <?xml version="1.0" encoding="UTF-8"?>
          <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" height="60" viewBox="0 0 96.81277 100">
            <defs>
              <style>
              #Calque_1 {
                margin-right: 1rem;
              }
                .cls-1 {
                  fill: #465da9;
                }
              </style>
            </defs>
            <path class="cls-1" d="m95.93156,53.13471c.13825-5.98964-.83158-11.83231-2.79387-17.30863l-10.6367,14.82384c.02799.72423.07266,1.44576.05585,2.17668-.21263,9.23728-4.01177,17.83963-10.6938,24.22095-6.68364,6.38102-15.45107,9.7782-24.689,9.56461-19.06562-.4408-34.22806-16.31252-33.78636-35.38295.43994-19.06993,16.31383-34.22616,35.38295-33.78636,1.90144.04396,3.77626.23974,5.61062.58145.63577.11843,1.26045.27472,1.88591.42794l8.23279-11.47354c-4.88059-1.7856-10.07094-2.78732-15.4212-2.91057C22.63521,3.45744.62394,24.47594.01296,50.92109c-.54959,23.81777,16.4423,44.03869,39.14762,48.26667,2.503.46627,5.07862.73868,7.70534.79912,12.80961.29575,24.96903-4.41485,34.23604-13.26369,9.26716-8.84964,14.53369-20.77807,14.8296-33.58848Z"/>
            <polygon class="cls-1" points="34.48314 41.30791 23.65825 49.16806 45.51999 79.28187 96.81277 7.80039 85.94202 0 45.45796 56.42278 34.48314 41.30791"/>
          </svg>
        `;
        let imgPlaceholder = document.querySelector(".page-title-wrapper");
        imgPlaceholder.insertAdjacentHTML('afterbegin', svgIcon);
    }

    // ______________________________________________________________________

    // PAGE INITIATIVES TITLE

    if (currentURL === "https://petitions.lecese.fr/pages/initiatives") {
        let antiFlexDiv = document.querySelector("h1.heading1.page-title")
        antiFlexDiv.style.marginBottom = "1rem"
        antiFlexDiv.classList.add("anti-flex-div")
        antiFlexDivBefore = window.getComputedStyle(antiFlexDiv, "::before")
        antiFlexDivHeight = antiFlexDiv.offsetHeight
        root = document.querySelector(":root");
        root.style.setProperty("--antiflexdivheight", (antiFlexDivHeight + 16)+"px");
        
        // __________________________________________________________________

        let svgIcon = `
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" height="60" viewBox="0 0 100 99.9979">
          <defs>
            <style>
              #Calque_1 {
                margin-right: 1rem;
              }
              .cls-1 {
                fill: #465da9;
              }
            </style>
          </defs>
          <path class="cls-1" d="m50.05968,99.9979c-.86601,0-1.73396-.02245-2.60583-.06639C19.92131,98.52658-1.33734,74.98529.06564,47.45177,1.47154,19.91825,25.03334-1.32575,52.54635.06453c13.33766.67952,25.6121,6.51309,34.56214,16.42578,8.95101,9.91171,13.50463,22.71629,12.8251,36.05396h0c-.6805,13.33766-6.51407,25.6121-16.42578,34.56214-9.2644,8.36521-21.05653,12.89149-33.44813,12.89149Zm-.08108-86.0021c-19.01893,0-34.95461,14.96425-35.93386,34.16868-1.01049,19.82538,14.29644,36.77742,34.12182,37.78791,19.81757,1.02514,36.77742-14.29448,37.78791-34.12084.49011-9.60417-2.78939-18.82367-9.23415-25.96162-6.44477-7.13698-15.28252-11.33715-24.88669-11.82629-.62095-.03222-1.23994-.04784-1.85503-.04784Z"/>
          <path class="cls-1" d="m36.74547,53.65009c0-10.96884,18.04723-9.26996,18.04722-16.34751,0-2.19326-1.69888-3.89214-4.45957-3.89214-3.11405,0-5.23596,2.05225-5.23595,5.30816,0,.70758.06965,1.34382.28202,1.9809h-9.83653c-.28201-.99044-.42472-2.05225-.42472-3.04269,0-6.86434,5.87473-12.52586,15.28654-12.52587,8.77473,0,14.36575,4.95309,14.36576,12.10113,0,12.52586-17.05508,11.88879-17.05507,15.92279,0,1.0618,1.0618,1.91124,3.46742,1.91124,3.11405,0,7.21855-1.34467,9.48316-2.75984v8.56237c-1.91124,1.0618-5.59102,2.12361-10.18989,2.12361-8.209,0-13.73037-3.2559-13.73037-9.34215Zm7.71463,17.4093c0-3.1132,2.33596-5.51967,5.51967-5.51967,3.11405,0,5.45002,2.40646,5.45002,5.51967,0,3.11405-2.33596,5.44917-5.45001,5.44917-3.18371,0-5.51967-2.33511-5.51967-5.44916Z"/>
        </svg>
        `;
        let imgPlaceholder = document.querySelector(".page-title-wrapper");
        imgPlaceholder.insertAdjacentHTML('afterbegin', svgIcon);
    }

    // PAGE TRAITEMENT TITLE

    if (currentURL === "https://petitions.lecese.fr/pages/traitement") {
        let antiFlexDiv = document.querySelector("h1.heading1.page-title")
        antiFlexDiv.style.marginBottom = "1rem"
        antiFlexDiv.classList.add("anti-flex-div")
        antiFlexDivBefore = window.getComputedStyle(antiFlexDiv, "::before")
        antiFlexDivHeight = antiFlexDiv.offsetHeight
        root = document.querySelector(":root");
        root.style.setProperty("--antiflexdivheight", (antiFlexDivHeight + 16)+"px");
                
        // __________________________________________________________________

        let svgIcon = `
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" height="60" viewBox="0 0 100 99.9979">
          <defs>
            <style>
              #Calque_1 {
                margin-right: 1rem;
              }
              .cls-1 {
                fill: #465da9;
              }
            </style>
          </defs>
          <path class="cls-1" d="m50.05968,99.9979c-.86601,0-1.73396-.02245-2.60583-.06639C19.92131,98.52658-1.33734,74.98529.06564,47.45177,1.47154,19.91825,25.03334-1.32575,52.54635.06453c13.33766.67952,25.6121,6.51309,34.56214,16.42578,8.95101,9.91171,13.50463,22.71629,12.8251,36.05396h0c-.6805,13.33766-6.51407,25.6121-16.42578,34.56214-9.2644,8.36521-21.05653,12.89149-33.44813,12.89149Zm-.08108-86.0021c-19.01893,0-34.95461,14.96425-35.93386,34.16868-1.01049,19.82538,14.29644,36.77742,34.12182,37.78791,19.81757,1.02514,36.77742-14.29448,37.78791-34.12084.49011-9.60417-2.78939-18.82367-9.23415-25.96162-6.44477-7.13698-15.28252-11.33715-24.88669-11.82629-.62095-.03222-1.23994-.04784-1.85503-.04784Z"/>
          <path class="cls-1" d="m36.74547,53.65009c0-10.96884,18.04723-9.26996,18.04722-16.34751,0-2.19326-1.69888-3.89214-4.45957-3.89214-3.11405,0-5.23596,2.05225-5.23595,5.30816,0,.70758.06965,1.34382.28202,1.9809h-9.83653c-.28201-.99044-.42472-2.05225-.42472-3.04269,0-6.86434,5.87473-12.52586,15.28654-12.52587,8.77473,0,14.36575,4.95309,14.36576,12.10113,0,12.52586-17.05508,11.88879-17.05507,15.92279,0,1.0618,1.0618,1.91124,3.46742,1.91124,3.11405,0,7.21855-1.34467,9.48316-2.75984v8.56237c-1.91124,1.0618-5.59102,2.12361-10.18989,2.12361-8.209,0-13.73037-3.2559-13.73037-9.34215Zm7.71463,17.4093c0-3.1132,2.33596-5.51967,5.51967-5.51967,3.11405,0,5.45002,2.40646,5.45002,5.51967,0,3.11405-2.33596,5.44917-5.45001,5.44917-3.18371,0-5.51967-2.33511-5.51967-5.44916Z"/>
        </svg>
        `;
        let imgPlaceholder = document.querySelector(".page-title-wrapper");
        imgPlaceholder.insertAdjacentHTML('afterbegin', svgIcon);
    }

    // PAGE REGLEMENT TITLE

    if (currentURL === "https://petitions.lecese.fr/pages/reglement") {
        let antiFlexDiv = document.querySelector("h1.heading1.page-title")
        antiFlexDiv.style.marginBottom = "1rem"
        antiFlexDiv.classList.add("anti-flex-div")
        antiFlexDivBefore = window.getComputedStyle(antiFlexDiv, "::before")
        antiFlexDivHeight = antiFlexDiv.offsetHeight
        root = document.querySelector(":root");
        root.style.setProperty("--antiflexdivheight", (antiFlexDivHeight + 16)+"px");
                
        // __________________________________________________________________

        let svgIcon = `
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" height="60" viewBox="0 0 100 99.9979">
          <defs>
            <style>
              #Calque_1 {
                margin-right: 1rem;
              }
              .cls-1 {
                fill: #465da9;
              }
            </style>
          </defs>
          <path class="cls-1" d="m50.05968,99.9979c-.86601,0-1.73396-.02245-2.60583-.06639C19.92131,98.52658-1.33734,74.98529.06564,47.45177,1.47154,19.91825,25.03334-1.32575,52.54635.06453c13.33766.67952,25.6121,6.51309,34.56214,16.42578,8.95101,9.91171,13.50463,22.71629,12.8251,36.05396h0c-.6805,13.33766-6.51407,25.6121-16.42578,34.56214-9.2644,8.36521-21.05653,12.89149-33.44813,12.89149Zm-.08108-86.0021c-19.01893,0-34.95461,14.96425-35.93386,34.16868-1.01049,19.82538,14.29644,36.77742,34.12182,37.78791,19.81757,1.02514,36.77742-14.29448,37.78791-34.12084.49011-9.60417-2.78939-18.82367-9.23415-25.96162-6.44477-7.13698-15.28252-11.33715-24.88669-11.82629-.62095-.03222-1.23994-.04784-1.85503-.04784Z"/>
          <path class="cls-1" d="m36.74547,53.65009c0-10.96884,18.04723-9.26996,18.04722-16.34751,0-2.19326-1.69888-3.89214-4.45957-3.89214-3.11405,0-5.23596,2.05225-5.23595,5.30816,0,.70758.06965,1.34382.28202,1.9809h-9.83653c-.28201-.99044-.42472-2.05225-.42472-3.04269,0-6.86434,5.87473-12.52586,15.28654-12.52587,8.77473,0,14.36575,4.95309,14.36576,12.10113,0,12.52586-17.05508,11.88879-17.05507,15.92279,0,1.0618,1.0618,1.91124,3.46742,1.91124,3.11405,0,7.21855-1.34467,9.48316-2.75984v8.56237c-1.91124,1.0618-5.59102,2.12361-10.18989,2.12361-8.209,0-13.73037-3.2559-13.73037-9.34215Zm7.71463,17.4093c0-3.1132,2.33596-5.51967,5.51967-5.51967,3.11405,0,5.45002,2.40646,5.45002,5.51967,0,3.11405-2.33596,5.44917-5.45001,5.44917-3.18371,0-5.51967-2.33511-5.51967-5.44916Z"/>
        </svg>
        `;
        let imgPlaceholder = document.querySelector(".page-title-wrapper");
        imgPlaceholder.insertAdjacentHTML('afterbegin', svgIcon);
    }
     
    // PAGE SIGNER TITLE

    if (currentURL === "https://petitions.lecese.fr/pages/signer-une-petition") {
        let antiFlexDiv = document.querySelector("h1.heading1.page-title")
        antiFlexDiv.style.marginBottom = "1rem"
        antiFlexDiv.classList.add("anti-flex-div")
        antiFlexDivBefore = window.getComputedStyle(antiFlexDiv, "::before")
        antiFlexDivHeight = antiFlexDiv.offsetHeight
        root = document.querySelector(":root");
        root.style.setProperty("--antiflexdivheight", (antiFlexDivHeight + 16)+"px");
                
        // __________________________________________________________________

        let svgIcon = `
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" height="60" viewBox="0 0 100 99.9979">
          <defs>
            <style>
              #Calque_1 {
                margin-right: 1rem;
              }
              .cls-1 {
                fill: #465da9;
              }
            </style>
          </defs>
          <path class="cls-1" d="m50.05968,99.9979c-.86601,0-1.73396-.02245-2.60583-.06639C19.92131,98.52658-1.33734,74.98529.06564,47.45177,1.47154,19.91825,25.03334-1.32575,52.54635.06453c13.33766.67952,25.6121,6.51309,34.56214,16.42578,8.95101,9.91171,13.50463,22.71629,12.8251,36.05396h0c-.6805,13.33766-6.51407,25.6121-16.42578,34.56214-9.2644,8.36521-21.05653,12.89149-33.44813,12.89149Zm-.08108-86.0021c-19.01893,0-34.95461,14.96425-35.93386,34.16868-1.01049,19.82538,14.29644,36.77742,34.12182,37.78791,19.81757,1.02514,36.77742-14.29448,37.78791-34.12084.49011-9.60417-2.78939-18.82367-9.23415-25.96162-6.44477-7.13698-15.28252-11.33715-24.88669-11.82629-.62095-.03222-1.23994-.04784-1.85503-.04784Z"/>
          <path class="cls-1" d="m36.74547,53.65009c0-10.96884,18.04723-9.26996,18.04722-16.34751,0-2.19326-1.69888-3.89214-4.45957-3.89214-3.11405,0-5.23596,2.05225-5.23595,5.30816,0,.70758.06965,1.34382.28202,1.9809h-9.83653c-.28201-.99044-.42472-2.05225-.42472-3.04269,0-6.86434,5.87473-12.52586,15.28654-12.52587,8.77473,0,14.36575,4.95309,14.36576,12.10113,0,12.52586-17.05508,11.88879-17.05507,15.92279,0,1.0618,1.0618,1.91124,3.46742,1.91124,3.11405,0,7.21855-1.34467,9.48316-2.75984v8.56237c-1.91124,1.0618-5.59102,2.12361-10.18989,2.12361-8.209,0-13.73037-3.2559-13.73037-9.34215Zm7.71463,17.4093c0-3.1132,2.33596-5.51967,5.51967-5.51967,3.11405,0,5.45002,2.40646,5.45002,5.51967,0,3.11405-2.33596,5.44917-5.45001,5.44917-3.18371,0-5.51967-2.33511-5.51967-5.44916Z"/>
        </svg>
        `;
        let imgPlaceholder = document.querySelector(".page-title-wrapper");
        imgPlaceholder.insertAdjacentHTML('afterbegin', svgIcon);
    }
     
    // PAGE SIGNALER TITLE

    if (currentURL === "https://petitions.lecese.fr/pages/signaler-une-petition") {
        let antiFlexDiv = document.querySelector("h1.heading1.page-title")
        antiFlexDiv.style.marginBottom = "1rem"
        antiFlexDiv.classList.add("anti-flex-div")
        antiFlexDivBefore = window.getComputedStyle(antiFlexDiv, "::before")
        antiFlexDivHeight = antiFlexDiv.offsetHeight
        root = document.querySelector(":root");
        root.style.setProperty("--antiflexdivheight", (antiFlexDivHeight + 16)+"px");
                
        // __________________________________________________________________

        let svgIcon = `
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" height="60" viewBox="0 0 100 99.9979">
          <defs>
            <style>
              #Calque_1 {
                margin-right: 1rem;
              }
              .cls-1 {
                fill: #465da9;
              }
            </style>
          </defs>
          <path class="cls-1" d="m50.05968,99.9979c-.86601,0-1.73396-.02245-2.60583-.06639C19.92131,98.52658-1.33734,74.98529.06564,47.45177,1.47154,19.91825,25.03334-1.32575,52.54635.06453c13.33766.67952,25.6121,6.51309,34.56214,16.42578,8.95101,9.91171,13.50463,22.71629,12.8251,36.05396h0c-.6805,13.33766-6.51407,25.6121-16.42578,34.56214-9.2644,8.36521-21.05653,12.89149-33.44813,12.89149Zm-.08108-86.0021c-19.01893,0-34.95461,14.96425-35.93386,34.16868-1.01049,19.82538,14.29644,36.77742,34.12182,37.78791,19.81757,1.02514,36.77742-14.29448,37.78791-34.12084.49011-9.60417-2.78939-18.82367-9.23415-25.96162-6.44477-7.13698-15.28252-11.33715-24.88669-11.82629-.62095-.03222-1.23994-.04784-1.85503-.04784Z"/>
          <path class="cls-1" d="m36.74547,53.65009c0-10.96884,18.04723-9.26996,18.04722-16.34751,0-2.19326-1.69888-3.89214-4.45957-3.89214-3.11405,0-5.23596,2.05225-5.23595,5.30816,0,.70758.06965,1.34382.28202,1.9809h-9.83653c-.28201-.99044-.42472-2.05225-.42472-3.04269,0-6.86434,5.87473-12.52586,15.28654-12.52587,8.77473,0,14.36575,4.95309,14.36576,12.10113,0,12.52586-17.05508,11.88879-17.05507,15.92279,0,1.0618,1.0618,1.91124,3.46742,1.91124,3.11405,0,7.21855-1.34467,9.48316-2.75984v8.56237c-1.91124,1.0618-5.59102,2.12361-10.18989,2.12361-8.209,0-13.73037-3.2559-13.73037-9.34215Zm7.71463,17.4093c0-3.1132,2.33596-5.51967,5.51967-5.51967,3.11405,0,5.45002,2.40646,5.45002,5.51967,0,3.11405-2.33596,5.44917-5.45001,5.44917-3.18371,0-5.51967-2.33511-5.51967-5.44916Z"/>
        </svg>
        `;
        let imgPlaceholder = document.querySelector(".page-title-wrapper");
        imgPlaceholder.insertAdjacentHTML('afterbegin', svgIcon);
    }

});