document.addEventListener("DOMContentLoaded", (event) => {
    if (window.location.href === 'https://pprod.marseille.k8s.osp.cat/processes/budgetparticipatif/f/14/budgets/1/voting') {
        let element = document.querySelector('.row.small-up-2.mediumlarge-up-3.large-up-4.card-grid.budget-category-voting');
        // Vérifie si l'élément existe avant de tenter de modifier la classe
        if (element) {
            // Remplace la classe 'large-up-4' par 'large-up-5'
            element.classList.replace('large-up-4', 'large-up-5');
        }
    }

    if (window.location.href.includes('https://pprod.marseille.k8s.osp.cat/processes/budgetparticipatif/f/14/budgets/2/voting')) {
        const categoriesToHide = ["Culture", "Nature en ville", "Solidarité et entraide", "Sport"];
        
        // Sélectionner tous les éléments de filtre
        const filters = document.querySelectorAll('.filters__has-subfilters label');

        filters.forEach(function(label) {
            // Vérifier si le texte du label est dans la liste des catégories à masquer
            const labelText = label.textContent.trim();
            if (categoriesToHide.includes(labelText)) {
                // Masquer le parent de l'élément label pour cacher l'ensemble du filtre
                label.parentElement.style.display = 'none';
            }
        });
    }
});