document.addEventListener("DOMContentLoaded", (event) => {
    let accueilNancy = "https://jeparticipe.metropolegrandnancy.fr/assemblies/ville-de-nancy";
    let budgetParticipatif = "https://jeparticipe.metropolegrandnancy.fr/processes_groups/3?filter%5Barea_id%5D=&filter%5Bdate%5D=all&filter%5Bscope_id%5D=";
    let ateliersVieQuartier = "https://jeparticipe.metropolegrandnancy.fr/assemblies/avq";        
    let concertations = "https://jeparticipe.metropolegrandnancy.fr/processes_groups/4";
    let assemblees = "https://jeparticipe.metropolegrandnancy.fr/processes_groups/5";
    let budgetParticipatifProject = "https://jeparticipe.metropolegrandnancy.fr/processes/budget-participatif-2025"

    // Fonction pour créer et insérer les boutons de navigation
    function createNavigationButtons() {
        let navigationButtons = document.createElement("div");
        navigationButtons.className = "row small-up-1 medium-up-4 card-grid";
        navigationButtons.innerHTML =
            `<div class="column">
                <a class="button card__button" href="`+budgetParticipatif+`" style="width: 100%;background-color: #0eb9d0;font-weight: 600;text-transform: uppercase;">Budget participatif</a>
            </div>
            <div class="column">
                <a class="button card__button" href="`+ateliersVieQuartier+`" style="width: 100%;background-color: #0eb9d0;font-weight: 600;text-transform: uppercase;">Ateliers de vie de quartier</a>
            </div>
            <div class="column">
                <a class="button card__button" href="`+concertations+`" style="width: 100%;background-color: #0eb9d0;font-weight: 600;text-transform: uppercase;">Concertations</a>
            </div>
            <div class="column">
                <a class="button card__button" href="`+assemblees+`" style="width: 100%;background-color: #0eb9d0;font-weight: 600;text-transform: uppercase;">Assemblées</a>
            </div>`;

        if (window.location.href === accueilNancy || window.location.href === ateliersVieQuartier) {
            let targetNavigationButtons = document.querySelector('#content .row.column .row');
            targetNavigationButtons.insertBefore(navigationButtons, targetNavigationButtons.firstChild);
        }

        if (window.location.href === budgetParticipatif ||
            window.location.href === concertations ||
            window.location.href === assemblees) {
            let targetNavigationButtons = document.querySelector('#content .row.column');
            targetNavigationButtons.insertBefore(navigationButtons, targetNavigationButtons.firstChild);
        }
    }

    // Vérifie l'URL actuelle et appelle la fonction appropriée pour créer les boutons de navigation
    if (window.location.href === accueilNancy ||
        window.location.href === budgetParticipatif ||
        window.location.href === ateliersVieQuartier ||
        window.location.href === concertations ||
        window.location.href === assemblees) {
        createNavigationButtons();
    }

    // Insérer la section des assemblées en vedette
    if (window.location.href === accueilNancy) {
        let highlightedAssembliesSection = document.createElement("section");
        highlightedAssembliesSection.id = "highlighted-assemblies";
        highlightedAssembliesSection.className = "row section";

        let cardContent = document.createElement("div");
        cardContent.className = "card card--full card--process";
        cardContent.innerHTML = `<div class="row collapse card--process__row">
                                    <div class="columns mediumlarge-8 large-6 card--process__column">
                                        <div class="card__content">
                                            <a class="card__link" href="`+budgetParticipatif+`">
                                            <h2 class="card__title">Budget Participatif</h2>
                                            </a>
                                            <div class="ql-editor-display"><p>Retrouvez dans cet espace numérique les concertations à enjeux métropolitains menées par la Ville de Nancy</p></div>
                                            <a class="button small hollow card__button" href="`+budgetParticipatif+`">Plus d'informations</a>
                                        </div>
                                    </div>
                                    <div class="columns mediumlarge-8 large-6 card--process__column">
                                        <div class="card--full__image" style="background-image:url('/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBcm9NIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--b147e6d56bedbfa3914180e75b5c4a1d6ed7f1e7/Sans-titre-10.png')">
                                            <div class="card__content row collapse">
                                                <div class="large-6 large-offset-6 columns">
                                                    <a class="button expanded button--sc" href="`+budgetParticipatifProject+`">Je participe</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>`;

        highlightedAssembliesSection.appendChild(cardContent);
        let targetHighlightedAssembliesSection = document.querySelector('.columns.medium-7.mediumlarge-8');
        targetHighlightedAssembliesSection.insertBefore(highlightedAssembliesSection, targetHighlightedAssembliesSection.firstChild);
    }


        // Vérifie si l'URL actuelle est celle spécifiée
        if (window.location.href === "https://jeparticipe.metropolegrandnancy.fr/assemblies/cartographie-bp") {
            // Masquer le bloc HTML contenant "Ceci est une assemblée privée"
            let callout = document.querySelector('div.callout.alert.callout--full');
            if (callout) {
                callout.style.display = 'none';
            }
    
            // Masquer le texte "Cartographie des projets lauréats des budgets participatifs des communes du Grand Nancy: Ceci est une assemblée privée, visible par les non-membres"
            let section = document.querySelector('div.section');
            if (section) {
                let paragraphs = section.querySelectorAll('div.ql-editor-display p');
                paragraphs.forEach(function (paragraph) {
                    if (paragraph.textContent.includes("Cartographie des projets lauréats des budgets participatifs des communes du Grand Nancy: Ceci est une assemblée privée, visible par les non-membres")) {
                        paragraph.style.display = 'none';
                    }
                });
    
                // Vérification et masquage du texte non encapsulé dans un paragraphe spécifique
                let strongElements = section.querySelectorAll('strong');
                strongElements.forEach(function (strongElement) {
                    if (strongElement.textContent.includes("Cartographie des projets lauréats des budgets participatifs des communes du Grand Nancy:")) {
                        let nextSibling = strongElement.nextSibling;
                        if (nextSibling && nextSibling.nodeType === Node.TEXT_NODE && nextSibling.textContent.includes("Ceci est une assemblée privée, visible par les non-membres")) {
                            strongElement.style.display = 'none';
                            nextSibling.textContent = nextSibling.textContent.replace("Ceci est une assemblée privée, visible par les non-membres", "");
                        }
                    }
                });
            }
        }
});