window.addEventListener("DOMContentLoaded", (event) => {

    let docs_containers = document.querySelectorAll('.docs__container')
    docs_containers.forEach(docs_container => {
        let button = docs_container.querySelector('button')
        let svg = docs_container.querySelector('svg')
        button.addEventListener("click", () => {
            if (button.getAttribute("aria-expanded").indexOf("true") > -1 ) {
                svg.style.transform = "rotate(90deg)";
            } else {
                svg.style.transform = "rotate(0deg)";
            }
        });
    });

});