window.addEventListener("DOMContentLoaded", (event) => {
    let currentURL = window.location.href
    if (currentURL === "https://jeparticipe.meyzieu.fr/users/sign_in") {
        let socialRegister = document.querySelector(".social-register")
        let a = socialRegister.querySelector("a.button.button--social.button--publik")
        let span = a.querySelector("span.button--social__text")

        span.innerHTML = "M'identifier avec mon compte \"Mon.Meyzieu.fr\""
    }
})