REGISTRY := rg.fr-par.scw.cloud
NAMESPACE := decidim
VERSION := latest
IMAGE_NAME := decidim-templates
TAG := $(REGISTRY)/$(NAMESPACE)/$(IMAGE_NAME):$(VERSION)

login:
	docker login $(REGISTRY) -u nologin -p $(SCW_SECRET_TOKEN)

build:
	docker build -t $(TAG) . --compress
push:
	@make login
	@make build
	docker push $(TAG)
pull:
	docker pull $(TAG)

bash:
	docker run --rm -it $(TAG) /bin/sh

run:
	@make build
	docker run -p 4000:80 --rm -it $(TAG)

apply:
	kubectl apply -f kube

setup:
	kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
	sleep 25
	@make apply

rollout:
	kubectl rollout restart deployment

destroy:
	kubectl delete all --all -n default
	kubectl delete all --all -n cert-manager

deploy:
	@make push
	@make apply
	@make rollout