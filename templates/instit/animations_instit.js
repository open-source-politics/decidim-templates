function cardAnimation() {
    processes = document.getElementsByClassName('card--process');
    for (let process of processes) {
        process.setAttribute("data-aos", "fade-up");
        process.setAttribute("data-aos-anchor-placement", "top-bottom");
        // process.classList.add("card--hover");
    }

    proposals = document.getElementsByClassName('card--proposal');
    for (let proposal of proposals) {
        proposal.setAttribute("data-aos", "fade-up");
        proposal.setAttribute("data-aos-anchor-placement", "top-bottom");
        // proposal.classList.add("card--hover");
    }

    assemblies = document.getElementsByClassName('card--assembly');
    for (let assembly of assemblies) {
        assembly.setAttribute("data-aos", "fade-up");
        assembly.setAttribute("data-aos-anchor-placement", "top-bottom");
        assembly.classList.add("card--hover");
    }

    activities = document.getElementsByClassName('card--activity');
    for (let activity of activities) {
        activity.setAttribute("data-aos", "fade-up");
        activity.setAttribute("data-aos-anchor-placement", "top-bottom");
        activity.classList.add("card--hover");
    }

    meetings = document.getElementsByClassName('card--meeting');
    for (let meeting of meetings) {
        meeting.setAttribute("data-aos", "fade-up");
        meeting.setAttribute("data-aos-anchor-placement", "top-bottom");
        // meeting.classList.add("card--hover");
    }

    events = document.getElementsByClassName('p-s');
    for (let event of events) {
        card = event.parentElement;
        card.classList.add("card--hover");
        card.setAttribute("data-aos", "fade-up");
        card.setAttribute("data-aos-anchor-placement", "top-bottom");
    }

    flashes = document.getElementsByClassName('flash callout');
    for (let flash of flashes) {
        flash.setAttribute("data-aos", "fade-right");
        flash.setAttribute("data-aos-delay", "500");
    }
}

window.addEventListener("DOMContentLoaded", (event) => {
    cardAnimation();
    AOS.init();
});