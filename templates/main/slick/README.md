## Slick theme
Demo : https://mehr-demokratie.dcd-commercial-sbox.osp.dev/
### Aesthetics
- Square shapes
- Sober, clean design
### Screenshot
![screenshot_slick](https://i.imgur.com/AFC5Efj.png)
