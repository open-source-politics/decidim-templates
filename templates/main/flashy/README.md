## Flashy theme
Demo : https://dcd-demo-commercial.osp.dev/
### Aesthetics
- Round shapes
- Bright colors
### Screenshot
![screenshot_flashy](https://imgur.com/tGImkkH.png)