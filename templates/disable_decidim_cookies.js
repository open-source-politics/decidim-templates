// Disable Decidim's default cookie banner
document.addEventListener("DOMContentLoaded", function () {
    // Version 0.27
    let cookiebanner_decidim_027 = document.querySelector('#dc-dialog-wrapper');
    if (cookiebanner_decidim_027 != undefined && cookiebanner_decidim_027 != null) {
        cookiebanner_decidim_027.parentElement.removeChild(cookiebanner_decidim_027);
    }

    // Versions before 0.27
    let cookiebanner_decidim = document.querySelector('.cookie-warning');
    if (cookiebanner_decidim != undefined && cookiebanner_decidim != null) {
        cookiebanner_decidim.parentElement.removeChild(cookiebanner_decidim);
    }
});