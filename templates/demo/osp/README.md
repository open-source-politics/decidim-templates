## OSP theme
Demo : https://demo.decidim.opensourcepolitics.eu/?locale=fr
### Aesthetics
- Roundy shapes
- Sober, clean design
- Main navbar left aligned
### Screenshot
![screenshot_slick](https://i.imgur.com/Pca7wzp.png)
